# Common JavaScript Question

I got all of this question from twitter posted by some programmer out there. 
I'm assumed they're very experienced in this. And also i determined to answer this question since it's been 
a while since I learn the basic of JavaScript. Hopefully this also help others that new to JavaScript. 
Hope you like it. Leave a star if you want it's free :smile: Here we go!

### 1. What is JavaScript?

JavaScript is a scripting programming language that allows you to implement complex feature on web pages. 
JavaScript is the third layer of the layer cake of standard web technologies (after HTML and CSS).
Source: [mdn web docs - what is javascript?](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/What_is_JavaScript)

### 2. What are the feature of JavaScript?

Some feature JavaScript has to offer :
- Store useful values in variables.
- Modify event of a websites component, e.g. click
- Multiple functionality presented by its API, e.g. DOM
- Use third party API.
- Present dynamic data on the web.
- Media presentation on web.
  
Source: [mdn web docs - what is javascript?](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/What_is_JavaScript)

### 3. What data types does JavaScript support?

The set of types in the JavaScript consist of `primitive values` and `objects`.

#### Primitive Values

- Boolean: logical entity, only have 2 value `true` and `false`.
- Null: has exactly one value `null`.
- Undefined: A variable that has not been assigned a value has the value of `undefined`.
- Number type: store floating point between `2^-1074 and 2^1024`, but can only safely store integers in the range `-(2^53 - 1) to 2^53 -1`.
- BigInt: With BigInts, you can safely store and operate on large integers even beyond the safe integer limit for Numbers.
- NaN: `Not a Number` the only JavaScript that is not equal to itself.
- String: used to represent textual data.
- Symbol type: may be used as the key of an Object property

#### Objects

Basically it's a collection of properties. The usage often to groups a variables that has the same context.
e.g. 
```
 const car = { 
    color: 'red', 
    age: 5, 
    type: 'SUV' 
 };
```
Source: [mdn web docs - JavaScript data types and data structures](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures)

### 4. What are the advantages of JavaScript?

1. Speed: JavaScript is an interpreted language so less time needed for compilation.
2. Simplicity: Easy to understand, the structure is simple.
3. Popularity: All modern browsers support JavaScript.
4. Interoperability: Works perfectly with other programming language, e.g. library bridging in react native.
5. Server Load: There is a possibility to validate data on client side rather than send the data to Back-End to avoid page reload.
6. Rich Interface: Provides a lot of opportunity to create catchy webpages.
7. Extended Functionality: A lot of add-ons that can help developer.
8. Versatility: Handle both back-end(using NodeJS) and front-end(ReactJS, AngularJS).
9. Less Overhead: Improve the performance of websites and web applications by reducing code length.

Source: [DataFlair - Pros and Cons of JavaScript](https://data-flair.training/blogs/advantages-disadvantages-javascript/)

### 5. What is the difference between Java & JavaScript?
| Key                    | Java                                                     | JavaScript                           |
|------------------------|----------------------------------------------------------|--------------------------------------|
| OOP Point Of View      | OOP Programming Language                                 | OOP Programming Script               |
| Compilation            | Compiled - need to be build to executed file before run. | Interpreted - Run the code as it is. |
| Variables Type         | Strongly Typed                                           | Dynamically Typed                    |
| Functional Programming | Start from version 1.8                                   | Functional by default                |

Source: [SoftwareGuild - What is the difference between Java and JavaScript?](https://www.thesoftwareguild.com/faq/difference-between-java-and-javascript/#:~:text=According%20to%20the%20Java%20platform,other%20hand%2C%20must%20be%20compiled.)

### 6. How can you create an object in JavaScript?

#### a. Using Object Initializers
```javascript
const obj = {
  property_1:   value_1,   // property name may be an identifier...
  2:            value_2,   // or a number...
  // ...,
  'property n': value_n    // or a string
};
```

#### b. Using a constructor function
```javascript
function Car(make, model, year) {
  this.make = make;
  this.model = model;
  this.year = year;
}

// Object instantiation
const kenscar = new Car('Nissan', '300ZX', 1992);
const vpgscar = new Car('Mazda', 'Miata', 1990);
```

Source: [mdn web docs - Working with objects](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Working_with_Objects)

### 7. How can you create an Array in JavaScript?

There are 3 ways to create an array in javascript which are using `array literal notation`, using `Array` construction 
and using `String.prototype.split()` to build array from a string. Below are the example of how to create an array.
```javascript
// ARRAY LITERAL NOTATION
const fruits = ['Apple', 'Banana'];
console.log(fruits.length); // 2

// ARRAY CONSTRUCTOR
const fruits = new Array('Apple', 'Banana');
console.log(fruits.length); // 2

// String.prototype.split()
const fruits = 'Apple, Banana'.split(', ');
console.log(fruits.length); // 2
```

Source: [mdn web docs - Array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)

### 8. Can you assign an anonymous function to a variable and pass it as an argument to another function?

Anonymous functions are functions that are dynamically declared at runtime.
They’re called anonymous functions because they aren’t given a name in the same way as normal functions.
Because functions are first-class objects, we can pass a function as an argument in another function and later execute 
that passed-in function or even return it to be executed later.
This is the essence of using callback functions in JavaScript.

Source: [ANONYMOUS FUNCTIONS, CALLBACKS AND CHAINING ESSENTIALS IN JS](https://simple-task.com/news/anonymous-functions-callback-chaining-essentials-js/#:~:text=They're%20called%20anonymous%20functions,it%20to%20be%20executed%20later.)

### 9. Is JavaScript a case-sensitive language?

JavaScript is a case-sensitive language. This means that language keywords, variables, function names, and any other 
identifiers must always be typed with a consistent capitalization of letters. The while keyword, for example, must be 
typed “while”, not “While” or “WHILE”.

Source: [JavaScript: The Definitive Guide, Fourth Edition by](https://www.oreilly.com/library/view/javascript-the-definitive/0596000480/ch02s02.html#:~:text=JavaScript%20is%20a%20case%2Dsensitive,While%E2%80%9D%20or%20%E2%80%9CWHILE%E2%80%9D.)

### 10. What is a Name function in JavaScript & how to define it?

